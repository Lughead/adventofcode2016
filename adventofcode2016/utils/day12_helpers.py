# Stdlib
from __future__ import print_function
import logging
# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class AssemblyProcessor:
    def __init__(self):
        log.info('Initializing a, b, c, d to zero')
        self.a = 0
        self.b = 0
        self.c = 0
        self.d = 0
        self.instructions = []
        self.i_ptr = 0

    def process(self):
        f = {'inc': self.inc,
             'dec': self.dec,
             'jnz': self.jnz,
             'cpy': self.cpy,
             }
        while self.i_ptr < len(self.instructions):
            cmd, *args = self.instructions[self.i_ptr].split()
            f[cmd](*args)
            self.i_ptr += 1

    def inc(self, *args):
        x = args[0]
        attr = self.__getattribute__(x)
        attr += 1
        self.__setattr__(x, attr)

    def dec(self, *args):
        x = args[0]
        attr = self.__getattribute__(x)
        attr -= 1
        self.__setattr__(x, attr)

    def jnz(self, *args):
        x = args[0]
        y = args[1]
        try:
            x = int(x)
        except:
            pass
        y = int(y)
        if isinstance(x, int):
            check = x
        else:
            check = self.__getattribute__(x)
        if check != 0:
            self.i_ptr += (y - 1)

    def cpy(self, *args):
        x = args[0]
        y = args[1]
        try:
            x = int(x)
        except:
            pass
        if isinstance(x, str):
            self.__setattr__(y, self.__getattribute__(x))
        else:
            self.__setattr__(y, x)

    @property
    def cur_instruction(self):
        return self.instructions[self.i_ptr]
