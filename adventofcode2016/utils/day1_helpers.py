"""
day1_helpers.py from adventofcode2016
Created: 12/1/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


AXIS = 'axis'
MULT = 'mult'
LEFT = 'left'
RIGHT = 'right'

# this represents north, east, south, and west. With a right-_turn, increment the index by 1 % len
DIRS = [[1, 1], [0, 1], [1, -1], [0, -1]]


class Day1ParsingError(Exception):
    pass


class Day1Santa:
    def __init__(self):
        self.coord = [0, 0]  # represents (x, y)
        self.dir_idx = 0
        self.nodes = [self.coord.copy()]
        self.first_repeat_distance = 0

    @property
    def direction(self):
        return DIRS[self.dir_idx]

    @property
    def distance(self):
        return abs(self.coord[0]) + abs(self.coord[1])  # |x| + |y|

    def _turn(self, direction, blocks):
        log.debug('Turning {} {} blocks'.format(direction, blocks))
        if direction == LEFT:
            self.dir_idx -= 1
            if self.dir_idx < 0:
                self.dir_idx = len(DIRS) - 1
        if direction == RIGHT:
            self.dir_idx += 1
            if self.dir_idx > len(DIRS) - 1:
                self.dir_idx = 0
        tuple_idx = DIRS[self.dir_idx][0]
        mult = DIRS[self.dir_idx][1]
        for i in range(blocks):
            self.coord[tuple_idx] += (1 * mult)
            log.debug('New coordinates are {}'.format(self.coord))
            if not self.first_repeat_distance and (self.coord in self.nodes):
                log.info('Found the first repeat node at {}, distance: {}'.format(self.coord, self.distance))
                self.first_repeat_distance = self.distance
            self.nodes.append(self.coord.copy())

    def turn_left(self, blocks):
        self._turn(LEFT, blocks)

    def turn_right(self, blocks):
        self._turn(RIGHT, blocks)


class Day1Parser:
    @staticmethod
    def parse(instructions: str):
        log.debug('Parsing instructions: {}'.format(instructions))
        if not instructions:
            raise Day1ParsingError('The instruction set was empty')
        l_instructions = [i.strip() for i in instructions.split(',')]
        santa = Day1Santa()
        for instr in l_instructions:
            try:
                direction, blocks = Day1Parser.split_instruction(instr)
            except Exception as e:
                log.error('There was an error parsing instruction [{}]'.format(instr))
                raise Day1ParsingError(str(e))
            if direction.lower() == 'l':
                santa.turn_left(blocks)
            elif direction.lower() == 'r':
                santa.turn_right(blocks)
            else:
                log.error('There was an error parsing instruction [{}]'.format(instr))
                raise Day1ParsingError('Instructions must begin with letters "L" or "R". Found "{}"'.format(direction))
        return santa

    @staticmethod
    def split_instruction(instr: str):
        direction = instr[0]
        blocks = int(instr[1:])
        return direction, blocks
