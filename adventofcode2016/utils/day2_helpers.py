"""
day2_helpers.py from adventofcode2016
Created: 12/1/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class Key:
    def __init__(self, value):
        self.value = str(value)
        self.left = None
        self.right = None
        self.up = None
        self.down = None

    def set_adjacents(self, up, down, left, right):
        self.up = up
        self.down = down
        self.left = left
        self.right = right


class Keypad:
    def __init__(self):
        self.keys = {}
        self.cur_key = None
        self.__init_keys()

    def __init_keys(self):
        for i in range(10):
            self.keys[i] = Key(i)
        self.keys[1].set_adjacents(up=self.keys[1], down=self.keys[4], left=self.keys[1], right=self.keys[2])
        self.keys[2].set_adjacents(up=self.keys[2], down=self.keys[5], left=self.keys[1], right=self.keys[3])
        self.keys[3].set_adjacents(up=self.keys[3], down=self.keys[6], left=self.keys[2], right=self.keys[3])
        self.keys[4].set_adjacents(up=self.keys[1], down=self.keys[7], left=self.keys[4], right=self.keys[5])
        self.keys[5].set_adjacents(up=self.keys[2], down=self.keys[8], left=self.keys[4], right=self.keys[6])
        self.keys[6].set_adjacents(up=self.keys[3], down=self.keys[9], left=self.keys[5], right=self.keys[6])
        self.keys[7].set_adjacents(up=self.keys[4], down=self.keys[7], left=self.keys[7], right=self.keys[8])
        self.keys[8].set_adjacents(up=self.keys[5], down=self.keys[8], left=self.keys[7], right=self.keys[9])
        self.keys[9].set_adjacents(up=self.keys[6], down=self.keys[9], left=self.keys[8], right=self.keys[9])
        self.cur_key = self.keys[5]

    def up(self):
        self.cur_key = self.cur_key.up

    def down(self):
        self.cur_key = self.cur_key.down

    def left(self):
        self.cur_key = self.cur_key.left

    def right(self):
        self.cur_key = self.cur_key.right


class DiamondKeypad(Keypad):
    def __init__(self):
        super(DiamondKeypad, self).__init__()
        self.keys = {}
        self.cur_key = None
        self.__init_keys()

    def __init_keys(self):
        for i in range(10):
            self.keys[i] = Key(i)
        for key in ['A', 'B', 'C', 'D']:
            self.keys[key] = Key(key)
        self.keys[1].set_adjacents(up=self.keys[1], down=self.keys[3], left=self.keys[1], right=self.keys[1])
        self.keys[2].set_adjacents(up=self.keys[2], down=self.keys[6], left=self.keys[2], right=self.keys[3])
        self.keys[3].set_adjacents(up=self.keys[1], down=self.keys[7], left=self.keys[2], right=self.keys[4])
        self.keys[4].set_adjacents(up=self.keys[4], down=self.keys[8], left=self.keys[3], right=self.keys[4])
        self.keys[5].set_adjacents(up=self.keys[5], down=self.keys[5], left=self.keys[5], right=self.keys[6])
        self.keys[6].set_adjacents(up=self.keys[2], down=self.keys['A'], left=self.keys[5], right=self.keys[7])
        self.keys[7].set_adjacents(up=self.keys[3], down=self.keys['B'], left=self.keys[6], right=self.keys[8])
        self.keys[8].set_adjacents(up=self.keys[4], down=self.keys['C'], left=self.keys[7], right=self.keys[9])
        self.keys[9].set_adjacents(up=self.keys[9], down=self.keys[9], left=self.keys[8], right=self.keys[9])
        self.keys['A'].set_adjacents(up=self.keys[6], down=self.keys['A'], left=self.keys['A'], right=self.keys['B'])
        self.keys['B'].set_adjacents(up=self.keys[7], down=self.keys['D'], left=self.keys['A'], right=self.keys['C'])
        self.keys['C'].set_adjacents(up=self.keys[8], down=self.keys['C'], left=self.keys['B'], right=self.keys['C'])
        self.keys['D'].set_adjacents(up=self.keys['B'], down=self.keys['D'], left=self.keys['D'], right=self.keys['D'])
        self.cur_key = self.keys[5]

    def up(self):
        self.cur_key = self.cur_key.up

    def down(self):
        self.cur_key = self.cur_key.down

    def left(self):
        self.cur_key = self.cur_key.left

    def right(self):
        self.cur_key = self.cur_key.right


class KeypadParser:
    @staticmethod
    def parse(keypad: Keypad, instructions: str):
        inputs = instructions.split('\n')
        log.info('Parsing instructions: {}'.format(instructions))
        log.info('# of key inputs: {}'.format(len(inputs)))
        passcode = ''
        keypad = keypad.__class__()
        for input_seq in inputs:
            for c in input_seq:
                c = c.upper()
                log.debug('current key: {}'.format(keypad.cur_key.value))
                log.debug('instruction character: {}'.format(c))
                if c == 'U':
                    keypad.cur_key = keypad.cur_key.up
                elif c == 'D':
                    keypad.cur_key = keypad.cur_key.down
                elif c == 'L':
                    keypad.cur_key = keypad.cur_key.left
                elif c == 'R':
                    keypad.cur_key = keypad.cur_key.right
                else:
                    log.error('Found an unexpected instruction: {}'.format(c))
                    raise ParsingError('The input must be L, R, U, or D. Input was: {}'.format(c))
                log.debug('new key: {}'.format(keypad.cur_key.value))
            passcode += keypad.cur_key.value
        return passcode
