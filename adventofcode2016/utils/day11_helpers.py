# Stdlib
from __future__ import print_function
import logging
# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'

MICROCHIP = 'M'
GENERATOR = 'G'


class ParsingError(Exception):
    pass


class StorageError(Exception):
    pass


class Equipment(object):
    def __init__(self, name: str='', compatibility: str=''):
        self.full_name = name
        self.compatible_type = compatibility
        self.short_name = self._upper_first(self.full_name) + self._upper_first(self.compatible_type)

    @classmethod
    def _upper_first(cls, s: str) -> str:
        if s:
            return s[0].upper()
        return ''

    def is_compatible(self, e):
        if not isinstance(e, Microchip) and not isinstance(e, Generator):
            raise ValueError('Invalid Equipment subclass found: {}'.format(type(e)))
        return e.compatible_type == self.compatible_type


    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)


class Microchip(Equipment):
    full_name = MICROCHIP
    def __init__(self, compatibility: str):
        super(Microchip, self).__init__(name=MICROCHIP, compatibility=compatibility)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)


class Generator(Equipment):
    full_name = GENERATOR
    def __init__(self, compatibility: str):
        super(Generator, self).__init__(name=GENERATOR, compatibility=compatibility)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)


class StorageClass(object):
    def __init__(self, equipment: list=None):
        if equipment is None:
            equipment = []
        self._equipment = []
        self._init_equipment(equipment)

    def _init_equipment(self, e_list: list):
        for e in e_list:
            if not isinstance(e, Equipment):
                raise StorageError('You cannot place non-Equipment on an Elevator')
            self._equipment.append(e)

    def accept(self, e: Equipment):
        self._equipment.append(e)

    def drop(self, e: Equipment):
        if e not in self._equipment:
            raise StorageError('The equipment was not on the elevator when it was dropped: {}'.format(e))
        self._equipment.remove(e)

    def item_count(self):
        return len(self._equipment)

    @property
    def equipment_list(self):
        return self._equipment

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)


class Floor(StorageClass):
    def __init__(self, level: int, equipment: list=None):
        super(Floor, self).__init__(equipment=equipment)
        self.level = level
        self.has_elevator = False

    def drop(self, e: Equipment):
        super(Floor, self).drop(e)

    def accept(self, e: Equipment):
        super(Floor, self).accept(e)

    def will_fry(self, chip: Microchip) -> bool:
        for g in [e for e in self.equipment_list if isinstance(e, Generator)]:
            if not g.compatible_type == chip.compatible_type:
                return True
        return False

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)


class Elevator(StorageClass):
    def __init__(self, starting_floor: Floor, equipment: list=None):
        super(Elevator, self).__init__(equipment=equipment)
        self.cur_floor = starting_floor
        self.moves = 0

    def drop(self, e: Equipment):
        if len(self.equipment_list) > 1:
            super(Elevator, self).drop(e)
            self.cur_floor.accept(e)
            log.info('Elevator is dropping {}'.format(e))
        else:
            log.info('Elevator cannot drop {}'.format(e))

    def accept(self, e: Equipment):
        if len(self.equipment_list) < 2:
            super(Elevator, self).accept(e)
            log.info('Elevator is accepting {}'.format(e))
            self.cur_floor.drop(e)
        else:
            log.info('Elevator is full. It cannot accept {}'.format(e))

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)


class Building:
    def __init__(self, num_floors: int=0, floors: list=None):
        if floors is None:
            floors = []
        if (floors and num_floors) and \
           (not len(floors) == num_floors):
            raise ValueError('The number of floors does not match the length of equipment list')
        self.num_objects = 0
        self.init_floors(num_floors, floors)
        self.validate_floors()
        self.elevator = Elevator(starting_floor=self.floors[0])
        self.elevator_floor = 0

    def init_floors(self, num_floors: int=0, floors: list=None):
        if not floors or floors is None:
            self.floors = []
            for i in range(num_floors):
                self.floors.append(Floor(level=i))
            self.num_objects = 0
        else:
            for f in floors:
                self.num_objects += f.item_count()
            self.floors = floors.copy()

    def validate_floors(self):
        for i in range(len(self.floors)):
            floor_copy = self.floors.copy()
            cur_floor = floor_copy.pop(i)
            for floor in floor_copy:
                if cur_floor == floor:
                    raise ValueError('There is a duplicate floor: {}'.format(i))

    @property
    def num_floors(self):
        return len(self.floors)

    def move_elevator(self, up: bool):
        if up:
            if self.elevator_floor == self.num_floors - 1:
                return
            else:
                self.elevator_floor += 1
        else:
            if self.elevator_floor == 0:
                return
            else:
                self.elevator_floor -= 1
        self.elevator.cur_floor = self.floors[self.elevator_floor]
        self.elevator.moves += 1

    def raise_all_to_top_level(self):
        next_gen = None
        while self.floors[-1].item_count != self.num_objects:
            # find next generator up
            for f in self.floors:
                for e in f.equipment_list:
                    if isinstance(e, Generator):
                        next_gen = f
                        break

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)
