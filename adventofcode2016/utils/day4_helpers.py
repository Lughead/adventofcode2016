# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016 import utils

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class Room:
    def __init__(self, s: str):
        self.input = s
        parts = self.input.split('-')
        self.encrypted_name = '-'.join(parts[:-1])
        self.sector, self.checksum = parts[-1].split('[')
        self.checksum = self.checksum.replace(']', '')
        self.char_count = self.order_chars(self.encrypted_name)
        self.name = self.decrypt_name(self.encrypted_name, int(self.sector))

    @classmethod
    def order_chars(cls, s):
        d = {}
        for c in [c for c in s if c != '-']:
            count = s.count(c)
            tmp = d.get(count, set([]))
            tmp.add(c)
            d[count] = tmp
        for key in d:
            tmp = sorted(d[key])
            d[key] = tmp
        return d

    @classmethod
    def decrypt_name(cls, encrypted_name: str, shift: int):
        s = ''
        for c in encrypted_name:
            if c == '-':
                s += ' '
            else:
                s += utils.cipher_shift(c, shift)
        return s

    @property
    def is_real(self):
        sorted_keys = sorted(self.char_count, reverse=True)
        checksum = ''
        for k in sorted_keys:
            if len(checksum) == 5:
                break
            for letter in self.char_count[k]:
                checksum += letter
                if len(checksum) == 5:
                    break
        return checksum == self.checksum

    def __repr__(self):
        d = self.__dict__
        d['is_real'] = self.is_real
        d['name'] = self.name
        return repr(d)


class Parser:
    @staticmethod
    def parse(inputs):
        lines = [l for l in inputs.split('\n') if l]
        rooms = []
        for line in lines:
            room = Room(line)
            rooms.append(room)
        return rooms
