# Stdlib
from __future__ import print_function
import logging
import hashlib
import string
# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class DoorCode:
    MAGIC = '00000'
    def __init__(self, salt: str):
        self.salt = salt
        self.max_index, self.password = self.compute_password(self.salt)

    @classmethod
    def compute_password(cls, salt: str, length: int=8):
        passwd = ''
        i = 0
        while len(passwd) < length:
            h = cls.get_salted_hash(salt, i)
            if h.startswith(cls.MAGIC):
                c = h[5]
                passwd += c
                log.debug('Found passcode character #{} at index {}: {} ({})'.format(len(passwd), i, c, passwd))
            i += 1
        log.info('Passcode for salt: {} is {} (found at index {})'.format(salt, passwd, i))
        return i, passwd

    @classmethod
    def get_salted_hash(cls, salt, i):
        s = '{}{}'.format(salt, i)
        m = hashlib.md5(s.encode())
        return m.hexdigest()

class ComplexDoorCode(DoorCode):
    INDX = 5
    CHR = 6
    DEF = '-'
    @classmethod
    def compute_password(cls, salt: str, length: int=8):
        passwd = [cls.DEF, cls.DEF, cls.DEF, cls.DEF, cls.DEF, cls.DEF, cls.DEF, cls.DEF]
        i = 0
        while cls.DEF in passwd:
            h = cls.get_salted_hash(salt, i)
            if h.startswith(cls.MAGIC):
                log.debug('Looking at hash: {} (index: {})'.format(h, i))
                if h[cls.INDX] in string.digits:
                    idx = int(h[cls.INDX])

                    if (0 <= idx < length) and passwd[idx] == cls.DEF:
                        passwd[idx] = h[cls.CHR]
                        log.debug('Set idx {} to chr {}: {}'.format(idx, h[cls.CHR], ''.join(passwd)))
            i += 1
        return i, ''.join(passwd)


class Parser:
    @staticmethod
    def parse(inputs):
        raise NotImplementedError()
