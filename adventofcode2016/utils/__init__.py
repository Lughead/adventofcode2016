"""
__init__.py.py from adventofcode2016
Created: 12/1/2016

Purpose:

Examples:

Usage:

"""
# Stdlib
from __future__ import print_function
import logging
import os
import string
import sys

# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def get_project_root() -> str:
    dir = os.path.abspath(os.path.split(__file__)[0])
    return os.path.join(dir, '../..')

def get_asset(path: str) -> str:
    asset_dir = os.path.join(get_project_root(), 'assets')
    full_path = os.path.join(asset_dir, path)
    with open(full_path) as f:
        return f.read()


def get_input(file_name: str) -> str:
    return get_asset(os.path.join('inputs', file_name))


def cipher_shift(c: str, n: int) -> str:
    alpha = string.ascii_lowercase
    letter_index = ord(c) - ord('a')
    return alpha[(letter_index + n) % len(alpha)]
