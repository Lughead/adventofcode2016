# Stdlib
from __future__ import print_function
import logging
# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class AddrParts:
    TRI = 3
    QUAD = 4

    def __init__(self, s: str):
        self.s = s
        self.has_mirror_quad = self.has_mirror_quad(self.s)
        self.aba_strings = self.find_aba_strings(self.s)

    @classmethod
    def has_mirror_quad(cls, s):
        """
        for any substr of len 4 in s, are any substr a mirror but not all the same (e.g., oxxo, but not aaaa)
        :param s: The str to examine
        :return: bool
        """
        for i in range(len(s)):
            quad = s[i:i+cls.QUAD]
            if len(quad) < cls.QUAD:
                return False
            pair1 = quad[0:2]
            pair2 = quad[2:4]
            pair2 = pair2[::-1]
            if (pair1 == pair2) and (pair1[0] != pair1[1]):
                return True

    @classmethod
    def find_aba_strings(cls, s):
        aba_strings = []
        for i in range(len(s)):
            tri = s[i:i+cls.TRI]
            if len(tri) < cls.TRI:
                break
            a = tri[0]
            b = tri[1]
            if tri == a+b+a and tri != a+a+a:
                aba_strings.append(tri)
        return aba_strings

    @classmethod
    def has_bab_string(cls, s, tri):
        a = tri[0]
        b = tri[1]
        target = b+a+b
        log.debug('Checking if s contains [{}]'.format(target))
        for i in range(len(s)):
            new_tri = s[i:i+cls.TRI]
            if len(new_tri) < cls.TRI:
                break
            if new_tri == target:
                log.debug('Found target at index {}'.format(i))
                return True
        return False

    def __str__(self):
        return self.s

    def __repr__(self):
        return repr(self.s)


class Ipv7Address:
    NHY = 'non_hypernet'
    HYP = 'hypernet'

    def __init__(self, addr: str):
        self.addr = addr
        self.supports_tls = self.calc_supports_tls(self.addr)
        self.supports_ssl = self.calc_supports_ssl(self.addr)

    @classmethod
    def get_parts(cls, addr: str) -> dict:
        """
        addr is a string sequence with some parts contained in []: aaaa[bbbb]ccccccccccccc[dddddddddd]eeeeeee
        :param addr: the model ipv7 address
        :return: dict
        """
        parts = {cls.NHY: [], cls.HYP: []}
        in_hypernet = False
        tmp = ''
        for i in range(len(addr)):
            if addr[i] in ['[', ']']:
                key = cls.NHY
                if in_hypernet:
                    key = cls.HYP
                parts[key].append(AddrParts(tmp))
                in_hypernet = ~in_hypernet
                tmp = ''
            else:
                tmp += addr[i]
            if i == len(addr) - 1:
                parts[cls.NHY].append(AddrParts(tmp))
                tmp = ''
        return parts

    @classmethod
    def calc_supports_tls(cls, addr: str) -> bool:
        parts = cls.get_parts(addr)
        mirror_in_nyh = False
        for addrpart in parts[cls.NHY]:
            mirror_in_nyh |= addrpart.has_mirror_quad
        mirror_in_hyp = False
        if mirror_in_nyh:
            for addrpart in parts[cls.HYP]:
                mirror_in_hyp |= addrpart.has_mirror_quad
        return mirror_in_nyh and not mirror_in_hyp

    @classmethod
    def calc_supports_ssl(cls, addr: str) -> bool:
        parts = cls.get_parts(addr=addr)
        for a_part in parts[cls.NHY]:
            for aba_tri in a_part.aba_strings:
                for b_part in parts[cls.HYP]:
                    if AddrParts.has_bab_string(b_part.s, aba_tri):
                        return True
        return False


class Parser:
    @classmethod
    def parse(cls, s):
        addrs = []
        for l in [l.strip() for l in s.split('\n') if l]:
            addrs.append(Ipv7Address(addr=l))
        return addrs
