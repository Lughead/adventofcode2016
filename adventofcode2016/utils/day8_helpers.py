# Stdlib
from __future__ import print_function
import logging
# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class Screen:
    def __init__(self, h: int=6, w: int=50):
        self.h = h
        self.w = w
        self.rows = self._init_rows(h=h, w=w)

    @staticmethod
    def _init_rows(h: int, w: int) -> list:
        rows = []
        for i in range(h):
            col = []
            for j in range(w):
                col.append(False)
            rows.append(col)
        return rows

    def rect(self, x: int, y: int):
        log.debug('Setting {}x{} rectange to True values'.format(x, y))
        for i in range(y):
            row = self.rows[i]
            for j in range(x):
                row[j] = True
            self.rows[i] = row

    def rotate_row(self, x: int, count: int):
        log.debug('Rotating row {} by {}'.format(x, count))
        self.rows[x] = Screen.shift(self.rows[x], count)

    def rotate_col(self, y: int, count: int):
        log.debug('Rotating column {} by {}'.format(y, count))
        col = []
        log.debug('Building vector from col {}'.format(y))
        for r in self.rows:
            col.append(r[y])
        col = Screen.shift(v=col, count=count)
        for i in range(len(col)):
            self.rows[i][y] = col[i]

    @property
    def lit_pixels(self) -> list:
        pixels = []
        for y in range(len(self.rows)):
            for x in range(len(self.rows[y])):
                if self.rows[y][x]:
                    pixels.append((x, y))
        return pixels

    @classmethod
    def shift(cls, v: list, count: int) -> list:
        edge = len(v)
        new_v = v.copy()
        cur_idx = 0
        for _ in v:
            idx_old = cur_idx
            idx_new = (cur_idx + count) % edge
            cur_idx = (cur_idx + 1) % edge
            new_v[idx_new] = v[idx_old]
        return new_v

    def copy(self):
        new_screen = Screen(h=self.h, w=self.w)
        new_screen.rows = self.rows.copy()
        return new_screen

    def __str__(self):
        t = '#'
        f = '.'
        out = ''
        for row in self.rows:
            for col in row:
                if col:
                    out += t
                else:
                    out += f
            out += '\n'
        return out


class Parser:
    @classmethod
    def parse(cls, s: str) -> Screen:
        screen = Screen()
        for line in [i.strip() for i in s.split('\n') if i]:
            if 'rect' in line:
                x, y = Parser.parse_rect(line)
                screen.rect(x=x, y=y)
            elif 'rotate' in line:
                index, count = Parser.parse_rotate(line)
                if 'column' in line:
                    screen.rotate_col(y=index, count=count)
                elif 'row' in line:
                    screen.rotate_row(x=index, count=count)
        return screen

    @classmethod
    def parse_rect(cls, s) -> tuple:
        _, grid = s.split()
        x, y = grid.split('x')
        return int(x), int(y)

    @classmethod
    def parse_rotate(cls, s: str) -> tuple:
        _, row_or_col, params = s.split(maxsplit=2)
        n_params = params[2:]
        log.debug('Stripping {} from {} ({})'.format(params[0:2], params, n_params))
        index, _, count = n_params.split()
        return int(index), int(count)
