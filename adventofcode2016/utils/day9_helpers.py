# Stdlib
from __future__ import print_function
import logging
import re
# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class Decompressor:
    @classmethod
    def decompress(cls, compressed: str) -> str:
        ptr = 0
        decompressed = ''
        while ptr < len(compressed):
            marker_start = compressed.find('(', ptr)
            if marker_start == -1:
                log.debug('No markers found')
                rest = compressed[ptr:]
                decompressed += rest
                ptr += len(rest)
            elif ptr <= marker_start:
                log.debug('Adding {} to decompressed'.format(compressed[ptr:marker_start]))
                decompressed += compressed[ptr:marker_start]
                marker_end = compressed.find(')', marker_start)
                marker_string = compressed[marker_start+1:marker_end]
                log.debug('Found marker: {}'.format(marker_string))
                chr_count, repeats = cls.split_marker(marker_string=marker_string)
                log.debug('Decompressing next {} characters {} times'.format(chr_count, repeats))
                ptr = marker_end + 1
                repeating_str = compressed[ptr:ptr+chr_count]
                expanded_str = cls.expand_string(repeating_str, repeats)
                log.debug('Adding {} to decompressed'.format(expanded_str))
                decompressed += expanded_str
                ptr += chr_count
        return decompressed

    RE_MARK = re.compile(r'\(\d+x\d+\)')
    @classmethod
    def find_marker(cls, s: str):
        m = cls.RE_MARK.findall(s)
        if not m:
            return ''
        return m[0]

    @classmethod
    def get_expanded_length(cls, s: str) -> int:
        cur_sz = 0
        if cls.has_markers(s):
            marker = cls.find_marker(s)
            before_len = s.find(marker)
            cur_sz += before_len
            chr_count, n_repeat = cls.split_marker_with_parens(marker_string=marker)
            idx = len(marker) + before_len
            repeating_str = s[idx:idx + chr_count]
            idx += chr_count  # move to point to after the marker
            s = s[idx:]
            cur_sz += (n_repeat * cls.get_expanded_length(s=repeating_str))
            cur_sz += cls.get_expanded_length(s=s)
        else:
            cur_sz += len(s)
        return cur_sz

    @classmethod
    def has_markers(cls, s: str) -> bool:
        m = cls.RE_MARK.search(s)
        return m is not None

    @classmethod
    def expand_string(cls, s, n):
        out = ''
        for i in range(n):
            out += s
        return out

    @classmethod
    def split_marker(cls, marker_string: str) -> tuple:
        chr_count, repeats = marker_string.split('x')
        return int(chr_count), int(repeats)

    @classmethod
    def split_marker_with_parens(cls, marker_string: str) -> tuple:
        return cls.split_marker(marker_string.replace('(', '').replace(')', ''))


class Parser:
    @classmethod
    def parse(cls, s):
        raise NotImplementedError()
