"""
day3_helpers.py from adventofcode2016
Created: 12/2/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class NumberTriple:
    def __init__(self, triple):
        if not len(triple) == 3:
            raise ValueError('The input must be of a list of length 3')
        self.a = int(triple[0])
        self.b = int(triple[1])
        self.c = int(triple[2])
        self.sorted_sides = sorted(self.triple)

    def is_triangle(self):
        shortest_sides = self.sorted_sides[0:2]
        extra_side = self.sorted_sides[2]
        return (shortest_sides[0] + shortest_sides[1]) > extra_side

    def __repr__(self):
        return str([self.a, self.b, self.c])

    @property
    def triple(self):
        return [self.a, self.b, self.c]


class Parser:
    @staticmethod
    def parse(inputs):
        triples = []
        for input_triple in inputs:
            t = NumberTriple(input_triple)
            is_triangle = t.is_triangle()
            triples.append({'triple': t, 'is_triangle': is_triangle})
        return triples


class ColumnParser:
    @staticmethod
    def parse(list_of_lists):
        columns = list(map(list, zip(*list_of_lists)))
        log.info('{} lists of {} elements each'.format(len(columns), len(columns[0])))
        triples = []
        for i in range(len(columns)):
            log.info('Processing column {}'.format(i))
            column = columns[i]
            j = 0
            while j < len(column):
                triple = column[j:j+3]
                nt = NumberTriple(triple=triple)
                triples.append(nt)
                j += 3
        return triples
