# Stdlib
from __future__ import print_function
import logging
import re
# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


HIGH_INIT = -1
LOW_INIT = 1000000

class ParsingError(Exception):
    pass


class Receiver(object):
    rcvr_type = 'receiver'

    def __init__(self, name: int=-1):
        self.name = name
        self.low = LOW_INIT
        self.high = HIGH_INIT
        self.chips = []

    def receives(self, chip_val: int):
        if chip_val < self.low:
            self.low = chip_val
        if chip_val > self.high:
            self.high = chip_val
        self.chips.append(chip_val)

    def __repr__(self) -> str:
        return '<{} name: {}, high: {}, low: {} />'.format(repr(self.rcvr_type), repr(self.name), repr(self.high),
                                                           repr(self.low))

    def __eq__(self, other) -> bool:
        return self.name == other.name

    def __ne__(self, other) -> bool:
        return self.name != other.name

    def __gt__(self, other) -> bool:
        return self.name > other.name

    def __le__(self, other) -> bool:
        return self.name < other.name


class Output(Receiver):
    rcvr_type = 'output'

    def __init__(self, name: int=-1):
        super(Output, self).__init__(name)

    def receives(self, chip_val: int):
        super(Output, self).receives(chip_val=chip_val)
        if chip_val in self.chips:
            raise RuntimeError('Multiple chip value {} found in {} {}'.format(chip_val, self.rcvr_type, self.name))
        self.chips.append(chip_val)


class Bot(Receiver):
    rcvr_type = 'bot'

    def __init__(self, name=-1):
        super(Bot, self).__init__(name=name)
        self.low_rcvr = None
        self.high_rcvr = None
        self.prev_high = 0
        self.prev_low = 0

    def gives(self, chip_val: int, receiver: Receiver):
        if chip_val == self.high:
            self.prev_high = self.high
            self.high = HIGH_INIT
        if chip_val == self.low:
            self.prev_low = self.low
            self.low = LOW_INIT
        self.chips.remove(chip_val)
        receiver.receives(chip_val)

    def receives(self, chip_val: int):
        super(Bot, self).receives(chip_val=chip_val)
        if self.low != LOW_INIT and self.high != HIGH_INIT:
            if self.low != self.high:
                if not (self.low_rcvr or self.high_rcvr):
                    raise NotImplementedError('There is no logic for no assigned receivers')
                self.execute_instruction()

    def execute_instruction(self):
        self.gives(self.high, self.high_rcvr)
        self.gives(self.low, self.low_rcvr)

    def chip_test(self):
        return [self.prev_low, self.prev_high]

    def __repr__(self):
        return "<Bot({}) low_v: {}, high_v: {}, low_rcv: {}, high_rcv: {} />".format(
            self.name, self.low, self.high, self.low_rcvr, self.high_rcvr
        )


class Factory:
    def __init__(self):
        self.bots = {}
        self.outputs = {}

    def add_output(self, output: Output):
        if output.name not in self.outputs:
            self.outputs[output.name] = output

    def add_bot(self, bot: Bot):
        if bot.name not in self.bots:
            self.bots[bot.name] = bot

    def give(self, chip_val: int, bot: Bot):
        bot.receives(chip_val)

    def execute_instruction(self, instr: str):
        if instr.startswith('value'):
            bot_name, chip_val = Parser.parse_input_intruction(instr)
            bot = self.get_bot(bot_name)
            bot.receives(chip_val=chip_val)
            self.bots[bot_name] = bot
        elif instr.startswith('bot'):
            bot_name, low_type, low_name, high_type, high_name = Parser.parse_give_instruction(instr)
            self.get_bot(bot_name=bot_name)
            bot = self.get_bot(bot_name)
            if low_type == Bot.rcvr_type:
                low_rcvr = self.get_bot(low_name)
            elif low_type == Output.rcvr_type:
                low_rcvr = self.get_output(low_name)
            else:
                raise ValueError('Could not process Receiver type: {}'.format(low_type))
            if high_type == Bot.rcvr_type:
                high_rcvr = self.get_bot(high_name)
            elif high_type == Output.rcvr_type:
                high_rcvr = self.get_output(high_name)
            else:
                raise ValueError('Could not process Receiver type: {}'.format(low_type))
            bot.low_rcvr = low_rcvr
            bot.high_rcvr = high_rcvr
            self.bots[bot_name] = bot

    def get_bot(self, bot_name: int):
        bot = self.bots.get(bot_name, Bot(name=bot_name))
        self.bots[bot.name] = bot
        return bot

    def get_output(self, out_name: int):
        out = self.outputs.get(out_name, Bot(name=out_name))
        self.outputs[out.name] = out
        return out

    def __repr__(self) -> str:
        return "<Factor bots={}, outputs={} />".format(self.bots, self.outputs)


class Parser:
    RE_INPUT = re.compile(r'value (\d+) goes to bot (\d+)')
    RE_GIVE = re.compile(r'bot (\d+) gives low to (\w+) (\d+) and high to (\w+) (\d+)')

    @classmethod
    def parse(cls, s: str) -> Factory:
        factory = Factory()
        instructions = [s.strip() for s in s.split('\n') if s]
        log.info('Read {} lines of instructions'.format(len(instructions)))
        assignment_instructions = [i for i in instructions if i.startswith('bot')]
        input_instructions = [i for i in instructions if i.startswith('value')]
        instructions = assignment_instructions + instructions
        for instr in instructions:
            factory.execute_instruction(instr=instr)
        return factory

    @classmethod
    def parse_input_intruction(cls, s: str) -> tuple:
        m = cls.RE_INPUT.match(s)
        if not m:
            raise ValueError('Missing expected regex from {}'.format(s))
        chip_val = int(m.group(1))
        bot_val = int(m.group(2))
        return bot_val, chip_val

    @classmethod
    def parse_give_instruction(cls, s: str) -> tuple:
        m = cls.RE_GIVE.search(s)
        if not m:
            raise ValueError('Expected format of instruction was not found')
        bot_name, low_type, low_val, high_type, high_val = \
            int(m.group(1)), m.group(2), int(m.group(3)), m.group(4), int(m.group(5))
        return bot_name, low_type, low_val, high_type, high_val
