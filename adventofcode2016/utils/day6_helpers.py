# Stdlib
from __future__ import print_function
import logging
from collections import Counter
# Third Party code
# Custom Code

log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParsingError(Exception):
    pass


class ArrayTransposer:
    def __init__(self, message_array: list, use_least_common: bool=False):
        self.message_array = message_array
        self.transposed_array = self.transpose_array(self.message_array)
        self.message = self.decode_message(self.transposed_array, use_least_common)

    @classmethod
    def transpose_array(cls, message_array: list):
        log.debug('Input: {}'.format(message_array))
        trans = list(map(list, zip(*message_array)))
        for i in range(len(trans)):
            trans[i] = ''.join(trans[i])
        log.debug('Output: {}'.format(trans))
        return trans

    @classmethod
    def decode_message(cls, trans: list, use_least_common: bool=False):
        message = []
        for i in range(len(trans)):
            counter = Counter(trans[i])
            if use_least_common:
                d = dict(counter)
                lowest = {'val': '-', 'count': 100000}
                for k, v in d.items():
                    if v < lowest['count']:
                        lowest = {'val': k, 'count': v}
                c = lowest['val']
            else:
                c, _ = counter.most_common()[0]
            message.append(c)
            log.debug('Setting idx {} to {}'.format(i, message[i]))
        return ''.join(message)


class Parser:
    @classmethod
    def parse(cls, in_str: str, use_least_common: bool=False):
        message_array = [l for l in in_str.split() if l]
        log.debug('Inputs: {}'.format(in_str))
        artra = ArrayTransposer(message_array, use_least_common=use_least_common)
        return artra
