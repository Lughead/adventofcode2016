# Advent of Code 2016 #

This project is to track the daily python challenges from http://adventofcode.com/2016

A new challenge every day. Up to 2 stars per day (2 part challenges) for completion. 
The sooner you complete the challenge after it's released, the higher on the leaderboard
you go.