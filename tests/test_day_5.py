# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day5_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'

P = 'passcode'
S = 'salt'


class DoorCodeTest(unittest.TestCase):
    def setUp(self):
        self.tests = [{S: 'abc', P: '18f47a30'},
                 ]

    @unittest.skip('Test is computationally and time intensive')
    def test_generate_passcode(self):
        for test in self.tests:
            idx, passwd = DoorCode.compute_password(test[S])
            self.assertEqual(passwd, test[P])


class ComplexDoorCodeTest(unittest.TestCase):
    def setUp(self):
        self.test = {S: 'abc', P: '05ace8e3'}

    @unittest.skip('Test is computationally and time intensive')
    def test_generate_passcode(self):
        idx, passwd = ComplexDoorCode.compute_password(self.test[S])
        self.assertEqual(passwd, self.test[P])
