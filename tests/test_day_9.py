# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day9_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class DecompressorTest(unittest.TestCase):
    C = 'compressed'
    D = 'decompressed'
    V2 = 'decompress_v2_len'

    def setUp(self):
        self.test_strings_v1 = [
            {self.C: 'ADVENT', self.D: 'ADVENT'},
            {self.C: 'A(1x5)BC', self.D: 'ABBBBBC'},
            {self.C: '(3x3)XYZ', self.D: 'XYZXYZXYZ'},
            {self.C: 'A(2x2)BCD(2x2)EFG', self.D: 'ABCBCDEFEFG'},
            {self.C: '(6x1)(1x3)A', self.D: '(1x3)A'},
            {self.C: 'X(8x2)(3x3)ABCY', self.D: 'X(3x3)ABC(3x3)ABCY'},
        ]
        self.test_strings_v2 = [
            {self.C: '(3x3)XYZ', self.V2: len('XYZXYZXYZ')},
            {self.C: 'X(8x2)(3x3)ABCY', self.V2: len('XABCABCABCABCABCABCY')},
            {self.C: '(27x12)(20x12)(13x14)(7x10)(1x12)A', self.V2: 241920},
            {self.C: '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN', self.V2: 445},
        ]

    def test_decompress(self):
        for test in self.test_strings_v1:
            decompressed = Decompressor.decompress(test[self.C])
            self.assertEqual(decompressed, test[self.D])

    def test_decompress_v2(self):
        for test in self.test_strings_v2:
            decompressed_len = Decompressor.get_expanded_length(test[self.C])
            self.assertEqual(decompressed_len, test[self.V2])
