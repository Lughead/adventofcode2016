# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day6_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'

TEST_INPUT = """eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar"""

ANSWER_MOST_COMMON = 'easter'
ANSWER_LEAST_COMMON = 'advent'


class ArrayTransposerTest(unittest.TestCase):
   def test_transposer(self):
       messages = [m for m in TEST_INPUT.split('\n') if m]
       art = ArrayTransposer(message_array=messages)
       self.assertEqual(art.message, ANSWER_MOST_COMMON)
       art = ArrayTransposer(message_array=messages, use_least_common=True)
       self.assertEqual(art.message, ANSWER_LEAST_COMMON)
