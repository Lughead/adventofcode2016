# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day12_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


INPUT = """"""


class AssemblyProcessorTest(unittest.TestCase):
    def setUp(self):
        self.ap = AssemblyProcessor()

    def test_inc(self):
        self.ap.a = 5
        self.assertEqual(self.ap.a, 5)
        self.ap.inc('a')
        self.ap.inc('b')
        self.assertEqual(self.ap.a, 6)
        self.assertEqual(self.ap.b, 1)
        self.assertEqual(self.ap.c, 0)
        self.assertEqual(self.ap.d, 0)

    def test_dec(self):
        self.ap.a = 5
        self.assertEqual(self.ap.a, 5)
        self.ap.dec('a')
        self.ap.dec('b')
        self.assertEqual(self.ap.a, 4)
        self.assertEqual(self.ap.b, -1)

    def test_jnz(self):
        instrs = ['inst 1',
                  'inst 2',
                  'inst 3',
                  'inst 4',
                  ]
        ptr = 0
        self.ap.a = 1
        self.ap.instructions = instrs
        self.assertEqual(self.ap.cur_instruction, instrs[0])
        jmp = 2
        ptr += jmp
        self.ap.jnz('a', jmp)
        self.assertEqual(self.ap.cur_instruction, instrs[ptr])
        jmp = -1
        ptr += jmp
        self.ap.jnz('a', jmp)
        self.assertEqual(self.ap.cur_instruction, instrs[ptr])
        jmp = -2
        ptr += jmp
        self.ap.jnz('a', jmp)
        self.assertEqual(self.ap.cur_instruction, instrs[ptr])
        self.ap.jnz(0, jmp)
        self.assertEqual(self.ap.cur_instruction, instrs[ptr])
        self.ap.jnz('c', 2)
        self.assertEqual(self.ap.cur_instruction, instrs[ptr])

    def test_cpy(self):
        self.ap.a = 1
        self.ap.b = 2
        self.ap.cpy('a', 'b')
        self.assertEqual(self.ap.a, self.ap.b)
        self.assertEqual(self.ap.a, 1)
        self.ap.cpy(41, 'a')
        self.assertEqual(41, self.ap.a)
        self.assertEqual(1, self.ap.b)

    def test_input(self):
        instrs = ['cpy 41 a',
                  'inc a',
                  'inc a',
                  'dec a',
                  'jnz a 2',
                  'dec a',
                  ]
        self.ap = AssemblyProcessor()
        self.ap.instructions = instrs
        self.ap.process()
        self.assertEqual(self.ap.a, 41 + 2 - 1)

