# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day8_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class TestScreen(unittest.TestCase):
    H = 3
    W = 7

    def setUp(self):
        self.test_screen = Screen(h=self.H, w=self.W)

    def test_screen_ctx(self):
        h = 6
        w = 50
        screen = Screen(h=h, w=w)
        self.assertEqual(len(screen.rows), h)
        for row in screen.rows:
            self.assertEqual(len(row), w)
            for col in row:
                self.assertFalse(col)

    def test_rect(self):
        screen = Screen(h=self.H, w=self.W)
        target = [
            [True, True, True, False, False, False, False],
            [True, True, True, False, False, False, False],
            [False, False, False, False, False, False, False],
        ]
        screen.rect(3, 2)
        self.assertListEqual(screen.rows, target)

    def test_shift(self):
        screen = self.test_screen.copy()
        screen.rect(3, 2)
        self.assertListEqual(Screen.shift(screen.rows[0], 1), [False, True, True, True, False, False, False])
        self.assertListEqual(Screen.shift(screen.rows[0], 5), [True, False, False, False, False, True, True])

    def test_print(self):
        screen = self.test_screen.copy()
        screen.rect(3, 2)
        self.assertEqual(str(screen), '###....\n###....\n.......\n')

    def test_rotate_col(self):
        screen = self.test_screen.copy()
        screen.rect(3, 2)
        screen.rotate_col(y=1, count=1)
        target = '#.#....\n###....\n.#.....\n'
        self.assertEqual(str(screen), target)
        screen.rotate_row(x=0, count=4)
        screen.rotate_col(y=1, count=1)
        target = '.#..#.#\n#.#....\n.#.....\n'
        self.assertEqual(str(screen), target)

    def test_rotate_row(self):
        screen = self.test_screen.copy()
        screen.rect(3,2)
        screen.rotate_col(y=1, count=1)
        screen.rotate_row(0, 4)
        target = '....#.#\n###....\n.#.....\n'
        self.assertEqual(str(screen), target)

    def test_lit_pixels(self):
        screen = Screen(h=self.H, w=self.W)
        self.assertEqual(len(screen.lit_pixels), 0)
        h = 3
        w = 2
        screen.rect(3, 2)
        self.assertEqual(len(screen.lit_pixels), h*w)


class ParserTest(unittest.TestCase):
    T = 'test'
    X = 'x'
    Y = 'y'

    def setUp(self):
        self.rect_test = {self.T: 'rect 3x2', self.X: 3, self.Y: 2}
        self.rotate_row_test = {self.T: 'rotate row y=0 by 4', self.X: 0, self.Y: 4}
        self.rotate_col_test = {self.T: 'rotate column x=1 by 1', self.X: 1, self.Y: 1}

    def test_parse_rect(self):
        x, y = Parser.parse_rect(self.rect_test[self.T])
        self.assertEqual(x, self.rect_test[self.X])
        self.assertEqual(y, self.rect_test[self.Y])

    def test_parse_rotate(self):
        x, y = Parser.parse_rotate(self.rotate_col_test[self.T])
        self.assertEqual(x, self.rotate_col_test[self.X])
        self.assertEqual(y, self.rotate_col_test[self.Y])
        x, y = Parser.parse_rotate(self.rotate_row_test[self.T])
        self.assertEqual(x, self.rotate_row_test[self.X])
        self.assertEqual(y, self.rotate_row_test[self.Y])
