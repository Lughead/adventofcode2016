# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day7_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'

P = 'part'
M = 'has_mirror'
A = 'has_aba'
Y = 'valid_bab_str'
N = 'invalid_bab_str'


class AddrPartsTest(unittest.TestCase):
    def setUp(self):
        self.quad_tests = [{P: 'abba', M: True},
                           {P: 'mnop', M: False},
                           {P: 'aaaa', M: False},
                           {P: 'ioxxoj', M: True},
                           ]
        self.tri_tests = [{P: 'aaabaaa', A: True, Y: 'awbbbab', N: 'caabj'},
                          {P: 'aaaaaaa', A: False},
                          {P: '', A: False},
                          {P: 'eweasdc', A: True, Y: 'awewbbab', N: 'caabj'},
                          {P: 'asdreewe', A: True, Y: 'wew', N: 'a'},
                          ]

    def test_has_mirror(self):
        for test in self.quad_tests:
            ap = AddrParts(test[P])
            self.assertEqual(ap.has_mirror_quad, test[M])

    def test_has_aba(self):
        for test in self.tri_tests:
            ap = AddrParts(test[P])
            self.assertEqual(bool(ap.aba_strings), test[A])
            for tri in ap.aba_strings:
                log.debug('Checking for bab({}) in {}'.format(tri, test[Y]))
                self.assertTrue(ap.has_bab_string(test[Y], tri))
                log.debug('Checking for bab({}) in {}'.format(tri, test[N]))
                self.assertFalse(ap.has_bab_string(test[N], tri))


class Ipv7Test(unittest.TestCase):
    I = 'ipv7'
    T = 'supports_tls'
    P = 'parts'
    N = Ipv7Address.NHY
    H = Ipv7Address.HYP
    A = 'aba_strings'
    S = 'supports_ssl'

    def setUp(self):
        self.tls_tests = [
            {self.I: 'abba[mnop]qrst', self.T: True, self.P: {self.N: ['abba', 'qrst'], self.H: ['mnop']}},
            {self.I: 'abcd[bddb]xyyx', self.T: False, self.P: {self.N: ['abcd', 'xyyx'], self.H: ['bddb']}},
            {self.I: 'aaaa[qwer]tyuself', self.T: False, self.P: {self.N: ['aaaa', 'tyuself'], self.H: ['qwer']}},
            {self.I: 'ioxxoj[asdfgh]zxcvbn', self.T: True, self.P: {self.N: ['ioxxoj', 'zxcvbn'], self.H: ['asdfgh']}},
        ]
        self.ssl_tests = [
            {self.I: 'aba[bab]xuw', self.S: True, self.A: ['aba']},
            {self.I: 'xyz[xyz]xyz', self.S: False, self.A: []},
            {self.I: 'aaa[aaa]bab', self.S: False, self.A: ['bab']},
            {self.I: 'bab[ewe]wew', self.S: True, self.A: ['bab', 'wew']},
            {self.I: 'aba[bab]xyz', self.S: True, self.A: ['aba']},
            {self.I: 'xyx[xyx]xyx', self.S: False, self.A: ['xyx', 'xyx']},
            {self.I: 'aaa[kek]eke', self.S: True, self.A: ['eke']},
            {self.I: 'zazbz[bzb]cdb', self.S: True, self.A: ['zaz', 'zbz']},
        ]

    def test_supports_tls(self):
        for test in self.tls_tests:
            addr = Ipv7Address(addr=test[self.I])
            self.assertEqual(addr.supports_tls, test[self.T])

    def test_supports_ssl(self):
        for test in self.ssl_tests:
            addr = Ipv7Address(addr=test[self.I])
            self.assertEqual(addr.supports_ssl, test[self.S])

    def test_get_parts(self):
        for test in self.tls_tests:
            parts_d = Ipv7Address.get_parts(test[self.I])
            # convert to string for comparison
            for key, o_list in parts_d.items():
                s_list = []
                for o in o_list:
                    s_list.append(str(o))
                parts_d[key] = s_list
            self.assertDictEqual(parts_d, test[self.P])

        for test in self.ssl_tests:
            parts_d = Ipv7Address.get_parts(test[self.I])
            # for k, o_list in parts_d.items():
            #     s_list = []
            #     for o in o_list:
            #         s_list.append(str(o))
            #     parts_d[k] = s_list
            aba_strings = []
            for addrpart in parts_d[self.N]:
                aba_strings += addrpart.aba_strings
            self.assertListEqual(aba_strings, test[self.A])
