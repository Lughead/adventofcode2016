# Stdlib
from __future__ import print_function
import logging
import unittest

# Third Party code
# Custom Code
from adventofcode2016.utils.day2_helpers import KeypadParser, Keypad, DiamondKeypad

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class ParserTest(unittest.TestCase):
    def test_parse(self):
        input = """ULL
RRDDD
LURDL
UUUUD"""
        output = '1985'
        self.assertEqual(KeypadParser.parse(Keypad(), input), output)
        output = '5DB3'
        self.assertEqual(KeypadParser.parse(DiamondKeypad(), input), output)


class KeypadTest(unittest.TestCase):
    def test_directions_good(self):
        keypad = Keypad()
        self.assertEqual(keypad.cur_key.value, '5')
        self.assertEqual(keypad.keys[1].up, keypad.keys[1])
        self.assertEqual(keypad.keys[1].down, keypad.keys[4])
        self.assertEqual(keypad.keys[1].left, keypad.keys[1])
        self.assertEqual(keypad.keys[1].right, keypad.keys[2])
        self.assertEqual(keypad.keys[2].up, keypad.keys[2])
        self.assertEqual(keypad.keys[2].down, keypad.keys[5])
        self.assertEqual(keypad.keys[2].left, keypad.keys[1])
        self.assertEqual(keypad.keys[2].right, keypad.keys[3])
        self.assertEqual(keypad.keys[3].up, keypad.keys[3])
        self.assertEqual(keypad.keys[3].down, keypad.keys[6])
        self.assertEqual(keypad.keys[3].left, keypad.keys[2])
        self.assertEqual(keypad.keys[3].right, keypad.keys[3])
        self.assertEqual(keypad.keys[4].up, keypad.keys[1])
        self.assertEqual(keypad.keys[4].down, keypad.keys[7])
        self.assertEqual(keypad.keys[4].left, keypad.keys[4])
        self.assertEqual(keypad.keys[4].right, keypad.keys[5])
        self.assertEqual(keypad.keys[5].up, keypad.keys[2])
        self.assertEqual(keypad.keys[5].down, keypad.keys[8])
        self.assertEqual(keypad.keys[5].left, keypad.keys[4])
        self.assertEqual(keypad.keys[5].right, keypad.keys[6])
        self.assertEqual(keypad.keys[6].up, keypad.keys[3])
        self.assertEqual(keypad.keys[6].down, keypad.keys[9])
        self.assertEqual(keypad.keys[6].left, keypad.keys[5])
        self.assertEqual(keypad.keys[6].right, keypad.keys[6])
        self.assertEqual(keypad.keys[7].up, keypad.keys[4])
        self.assertEqual(keypad.keys[7].down, keypad.keys[7])
        self.assertEqual(keypad.keys[7].left, keypad.keys[7])
        self.assertEqual(keypad.keys[7].right, keypad.keys[8])
        self.assertEqual(keypad.keys[8].up, keypad.keys[5])
        self.assertEqual(keypad.keys[8].down, keypad.keys[8])
        self.assertEqual(keypad.keys[8].left, keypad.keys[7])
        self.assertEqual(keypad.keys[8].right, keypad.keys[9])
        self.assertEqual(keypad.keys[9].up, keypad.keys[6])
        self.assertEqual(keypad.keys[9].down, keypad.keys[9])
        self.assertEqual(keypad.keys[9].left, keypad.keys[8])
        self.assertEqual(keypad.keys[9].right, keypad.keys[9])


class DiamondKeypadTest(unittest.TestCase):
    def test_moves(self):
        keypad = DiamondKeypad()
        self.assertEqual(keypad.cur_key.value, '5')
        self.assertEqual(keypad.keys[1].up, keypad.keys[1])
        self.assertEqual(keypad.keys[1].down, keypad.keys[3])
        self.assertEqual(keypad.keys[1].left, keypad.keys[1])
        self.assertEqual(keypad.keys[1].right, keypad.keys[1])
        self.assertEqual(keypad.keys[2].up, keypad.keys[2])
        self.assertEqual(keypad.keys[2].down, keypad.keys[6])
        self.assertEqual(keypad.keys[2].left, keypad.keys[2])
        self.assertEqual(keypad.keys[2].right, keypad.keys[3])
        self.assertEqual(keypad.keys[3].up, keypad.keys[1])
        self.assertEqual(keypad.keys[3].down, keypad.keys[7])
        self.assertEqual(keypad.keys[3].left, keypad.keys[2])
        self.assertEqual(keypad.keys[3].right, keypad.keys[4])
        self.assertEqual(keypad.keys[4].up, keypad.keys[4])
        self.assertEqual(keypad.keys[4].down, keypad.keys[8])
        self.assertEqual(keypad.keys[4].left, keypad.keys[3])
        self.assertEqual(keypad.keys[4].right, keypad.keys[4])
        self.assertEqual(keypad.keys[5].up, keypad.keys[5])
        self.assertEqual(keypad.keys[5].down, keypad.keys[5])
        self.assertEqual(keypad.keys[5].left, keypad.keys[5])
        self.assertEqual(keypad.keys[5].right, keypad.keys[6])
        self.assertEqual(keypad.keys[6].up, keypad.keys[2])
        self.assertEqual(keypad.keys[6].down, keypad.keys['A'])
        self.assertEqual(keypad.keys[6].left, keypad.keys[5])
        self.assertEqual(keypad.keys[6].right, keypad.keys[7])
        self.assertEqual(keypad.keys[7].up, keypad.keys[3])
        self.assertEqual(keypad.keys[7].down, keypad.keys['B'])
        self.assertEqual(keypad.keys[7].left, keypad.keys[6])
        self.assertEqual(keypad.keys[7].right, keypad.keys[8])
        self.assertEqual(keypad.keys[8].up, keypad.keys[4])
        self.assertEqual(keypad.keys[8].down, keypad.keys['C'])
        self.assertEqual(keypad.keys[8].left, keypad.keys[7])
        self.assertEqual(keypad.keys[8].right, keypad.keys[9])
        self.assertEqual(keypad.keys[9].up, keypad.keys[9])
        self.assertEqual(keypad.keys[9].down, keypad.keys[9])
        self.assertEqual(keypad.keys[9].left, keypad.keys[8])
        self.assertEqual(keypad.keys[9].right, keypad.keys[9])
        self.assertEqual(keypad.keys['A'].up, keypad.keys[6])
        self.assertEqual(keypad.keys['A'].down, keypad.keys['A'])
        self.assertEqual(keypad.keys['A'].left, keypad.keys['A'])
        self.assertEqual(keypad.keys['A'].right, keypad.keys['B'])
        self.assertEqual(keypad.keys['B'].up, keypad.keys[7])
        self.assertEqual(keypad.keys['B'].down, keypad.keys['D'])
        self.assertEqual(keypad.keys['B'].left, keypad.keys['A'])
        self.assertEqual(keypad.keys['B'].right, keypad.keys['C'])
        self.assertEqual(keypad.keys['C'].up, keypad.keys[8])
        self.assertEqual(keypad.keys['C'].down, keypad.keys['C'])
        self.assertEqual(keypad.keys['C'].left, keypad.keys['B'])
        self.assertEqual(keypad.keys['C'].right, keypad.keys['C'])
        self.assertEqual(keypad.keys['D'].up, keypad.keys['B'])
        self.assertEqual(keypad.keys['D'].down, keypad.keys['D'])
        self.assertEqual(keypad.keys['D'].left, keypad.keys['D'])
        self.assertEqual(keypad.keys['D'].right, keypad.keys['D'])
