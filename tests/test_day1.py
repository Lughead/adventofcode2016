# Stdlib
from __future__ import print_function
import logging
import unittest

# Third Party code
# Custom Code
from adventofcode2016.utils.day1_helpers import Day1Parser, Day1ParsingError

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


class Day1ParserTestCase(unittest.TestCase):
    def test_split_instruction_good(self):
        log.info('Running test_split_instruction_good')
        tests = [{'test': 'l1', 'direction': 'l', 'blocks': 1},
                 {'test': 'L1', 'direction': 'L', 'blocks': 1},
                 {'test': 'r1', 'direction': 'r', 'blocks': 1},
                 {'test': 'R1', 'direction': 'R', 'blocks': 1},
                 {'test': 'L12312', 'direction': 'L', 'blocks': 12312},
                 ]
        for test in tests:
            log.debug('Testing [{}]'.format(test))
            direction, blocks = Day1Parser.split_instruction(test['test'])
            self.assertEqual(direction, test['direction'])
            self.assertEqual(blocks, test['blocks'])

    def test_split_instruction_bad(self):
        log.info('Running test_split_instruction_bad')
        tests = ['b', '2L', 'la', 'L12L12', '-', 'L1-2', '']
        for test in tests:
            with self.assertRaises(Exception):
                log.debug('Testing [{}]'.format(test))
                Day1Parser.split_instruction(test)

    def test_parse_good(self):
        log.info('Running test_parse_good')
        # test that a list of instructions is properly split
        tests = [{'instructions': 'R2, L3', 'distance': 5},
                 {'instructions': 'R2, R2, R2', 'distance': 2},
                 {'instructions': 'R5, L5, R5, R3', 'distance': 12},
                 {'instructions': 'L1, L2, R1, R2, L123, R5345', 'distance': abs(-125) + abs(5345)},
                 ]
        for test in tests:
            log.debug('Testing [{}]'.format(test))
            santa = Day1Parser.parse(test['instructions'])
            self.assertEqual(santa.distance, test['distance'])

    def test_parse_bad(self):
        log.info('Running test_parse_bad')
        tests = ['R2, L3, RL3',
                 '35, R1',
                 'R1 35',
                 'A L1',
                 'L1 A',
                 '',
                 '~!@#',
                 ]
        for test in tests:
            log.debug('Testing [{}]'.format(test))
            with self.assertRaises(Day1ParsingError):
                Day1Parser.parse(test)

    def test_repeat_corner(self):
        input = 'R8, R4, R4, R8'
        answer = 4
        santa = Day1Parser.parse(input)
        self.assertEqual(santa.first_repeat_distance, answer)
