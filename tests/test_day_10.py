# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day10_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'

T = 'test'
P = 'parts'

INPUT = """value 5 goes to bot 2
bot 2 gives low to bot 1 and high to bot 0
value 3 goes to bot 1
bot 1 gives low to output 1 and high to bot 0
bot 0 gives low to output 2 and high to output 0
value 2 goes to bot 2"""


class BotTest(unittest.TestCase):
    def setUp(self):
        self.bot0 = Bot(0)
        self.bot1 = Bot(1)
        self.bot2 = Bot(2)
        self.bot3 = Bot(3)

    def test_receives(self):
        low_val = 1
        hi_val = 5
        mid_val = 3
        self.bot0.receives(low_val)
        self.assertTrue(low_val in self.bot0.chips)
        self.assertTrue(self.bot0.low == low_val)
        self.assertTrue(self.bot0.high == low_val)
        self.bot0.high_rcvr = self.bot1
        self.bot0.low_rcvr = self.bot2
        self.bot0.receives(hi_val)  # this SHOULD trigger the dispersion of chips
        self.assertEqual(self.bot0.chips, [])
        self.assertEqual(self.bot1.low, self.bot1.high)
        self.assertEqual(hi_val, self.bot1.high)
        self.assertEqual(self.bot2.low, self.bot2.high)
        self.assertEqual(low_val, self.bot2.high)
        self.bot1.high_rcvr = self.bot0
        self.bot1.low_rcvr = self.bot3
        self.bot1.receives(mid_val)
        self.assertEqual(self.bot1.chips, [])
        self.assertTrue(hi_val in self.bot0.chips)
        self.assertTrue(mid_val in self.bot3.chips)


class FactoryTest(unittest.TestCase):
    def test_get_bot(self):
        factory = Factory()
        factory.add_bot(Bot(2))
        bot = factory.get_bot(2)
        self.assertEqual(factory.get_bot(2), bot)
        bot.receives(1)
        self.assertEqual(factory.get_bot(2), bot)
        bot.low_rcvr = factory.get_bot(1)
        bot.high_rcvr = factory.get_bot(3)
        self.assertEqual(factory.get_bot(2), bot)
        bot2 = factory.get_bot(2)
        bot2.receives(2)
        self.assertEqual(factory.get_bot(2), bot)


class ParseTest(unittest.TestCase):
    def setUp(self):
        self.instructions = [
            {T: 'value 5 goes to bot 2', P: (2, 5)},
            {T: 'bot 2 gives low to bot 1 and high to bot 0', P: (2, 'bot', 1, 'bot', 0)},
            {T: 'value 3 goes to bot 1', P: (1, 3)},
            {T: 'bot 1 gives low to output 1 and high to bot 0', P: (1, 'output', 1, 'bot', 0)},
            {T: 'bot 0 gives low to output 2 and high to output 0', P: (0, 'output', 2, 'output', 0)},
            {T: 'value 2 goes to bot 2', P: (2, 2)},
        ]

    def test_parse_give_instruction(self):
        for i in [inst for inst in self.instructions if inst[T].startswith('bot')]:
            self.assertTupleEqual(i[P], Parser.parse_give_instruction(i[T]))

    def test_parse_input_intruction(self):
        for i in [inst for inst in self.instructions if inst[T].startswith('value')]:
            self.assertTupleEqual(i[P], Parser.parse_input_intruction(i[T]))

    def test_parse(self):
        # factory = Parser.parse(INPUT)
        instrs = [i[T] for i in self.instructions]
        f = Factory()
        bot0 = f.get_bot(0)
        bot1 = f.get_bot(1)
        bot2 = f.get_bot(2)
        output0 = f.get_output(0)
        output1 = f.get_output(1)
        output2 = f.get_output(2)
        f.execute_instruction('value 5 goes to bot 2')
        self.assertIn(5, bot2.chips)
        self.assertEqual(5, bot2.high)
        self.assertEqual(5, bot2.low)
        f.execute_instruction('bot 2 gives low to bot 1 and high to bot 0')
        self.assertTrue(bot2.low_rcvr == bot1)
        self.assertTrue(bot2.high_rcvr == bot0)
        f.execute_instruction('value 3 goes to bot 1')
        self.assertIn(3, bot1.chips)
        self.assertEqual(3, bot1.high)
        self.assertEqual(3, bot1.low)
        f.execute_instruction('bot 1 gives low to output 1 and high to bot 0')
        self.assertTrue(bot1.low_rcvr == output1)
        self.assertTrue(bot1.high_rcvr == bot0)
        f.execute_instruction('bot 0 gives low to output 2 and high to output 0')
        self.assertTrue(bot0.low_rcvr == output2)
        self.assertTrue(bot0.high_rcvr == output0)
        f.execute_instruction('value 2 goes to bot 2')  # triggers "giving"
        self.assertTrue(5 in output0.chips)
        self.assertTrue(2 in output1.chips)
        self.assertTrue(3 in output2.chips)
        self.assertSetEqual({2, 5}, set(bot2.chip_test()))
