# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day3_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'

TEST = 'test'
IS_TRIANGLE = 'is_triangle'
SHORTEST = 'shortest_sides'
TRIPLES = 'triples'


class NumberTripleTest(unittest.TestCase):
    def setUp(self):
        self.good_tests = [{TEST: [1, 2, 3], IS_TRIANGLE: False, SHORTEST: [1, 2]},
                      {TEST: [123, 23423413, 6661], IS_TRIANGLE: False, SHORTEST: [123, 6661]},
                      {TEST: [0, 0, 0], IS_TRIANGLE: False, SHORTEST: [0, 0]},
                      {TEST: [5, 5, 3], IS_TRIANGLE: True, SHORTEST: [3, 5]},
                      {TEST: [2, 2, 3], IS_TRIANGLE: True, SHORTEST: [2, 2]},
                      ]

        self.bad_tests = [['a', 1, 2],
                     [],
                     [1, 2],
                     [1],
                     [1, 2, 3, 4],
                     ]

    def test_ctx_good(self):
        for test in self.good_tests:
            self.assertIsInstance(NumberTriple(test[TEST]), NumberTriple)

    def test_is_triangle_good(self):
        for test in self.good_tests:
            triple = NumberTriple(triple=test[TEST])
            self.assertEqual(triple.is_triangle(), test[IS_TRIANGLE])

    def test_shortest_good(self):
        for test in self.good_tests:
            triple = NumberTriple(triple=test[TEST])
            self.assertEqual(triple.sorted_sides[0:2], test[SHORTEST])


class ParserTest(unittest.TestCase):
    def test_parse(self):
        self.skipTest('Day 3 has not been implemented')


class KeypadTest(unittest.TestCase):
    pass
