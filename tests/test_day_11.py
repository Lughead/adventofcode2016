# Stdlib
from __future__ import print_function

import unittest

from adventofcode2016.utils.day11_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


INPUT = """The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
The second floor contains a hydrogen generator.
The third floor contains a lithium generator.
The fourth floor contains nothing relevant."""


class BuildingTest(unittest.TestCase):
    def test_input(self):
        f1 = Floor(level=1, equipment=[Microchip(compatibility='lithium'), Microchip(compatibility='hydrogen')])
        f2 = Floor(level=2, equipment=[Generator(compatibility='hydrogen')])
        f3 = Floor(level=3, equipment=[Generator(compatibility='lithium')])
        f4 = Floor(level=4, equipment=[])
        bldg = Building(floors=[f1, f2, f3, f4])


    def test_move(self):
        f1 = Floor(level=1, equipment=[Microchip(compatibility='lithium'), Microchip(compatibility='hydrogen')])
        f2 = Floor(level=2, equipment=[Generator(compatibility='hydrogen')])
        f3 = Floor(level=3, equipment=[Generator(compatibility='lithium')])
        f4 = Floor(level=4, equipment=[])
        bldg = Building(floors=[f1, f2, f3, f4])
        self.assertEqual(bldg.num_objects, 4)
        num_moves = 0
        self.assertEqual(bldg.elevator.cur_floor, f1)
        self.assertEqual(bldg.elevator.cur_floor, bldg.floors[0])
        self.assertEqual(bldg.elevator.moves, num_moves)
        bldg.move_elevator(up=True)
        num_moves += 1
        self.assertEqual(bldg.elevator.moves, num_moves)
        self.assertEqual(bldg.elevator.cur_floor, f2)
        bldg.move_elevator(up=True)
        num_moves += 1
        self.assertEqual(bldg.elevator.moves, num_moves)
        self.assertEqual(bldg.elevator.cur_floor, f3)
        bldg.move_elevator(up=True)
        num_moves += 1
        self.assertEqual(bldg.elevator.moves, num_moves)
        self.assertEqual(bldg.elevator.cur_floor, f4)
        bldg.move_elevator(up=True)
        self.assertEqual(bldg.elevator.moves, num_moves)
        self.assertEqual(bldg.elevator.cur_floor, f4)
        bldg.move_elevator(up=False)
        num_moves += 1
        self.assertEqual(bldg.elevator.moves, num_moves)
        self.assertEqual(bldg.elevator.cur_floor, f3)
        bldg.move_elevator(up=False)
        num_moves += 1
        self.assertEqual(bldg.elevator.moves, num_moves)
        self.assertEqual(bldg.elevator.cur_floor, f2)
        bldg.move_elevator(up=False)
        num_moves += 1
        self.assertEqual(bldg.elevator.moves, num_moves)
        self.assertEqual(bldg.elevator.cur_floor, f1)
        bldg.move_elevator(up=False)
        self.assertEqual(bldg.elevator.moves, num_moves)
        self.assertEqual(bldg.elevator.cur_floor, f1)

    def test_building_ctx(self):
        n = 3
        bldg = Building(num_floors=n)
        self.assertEqual(bldg.num_floors, n)
        for floor in bldg.floors:
            self.assertIsInstance(floor, Floor)
        self.assertEqual(bldg.elevator.cur_floor, bldg.floors[0])

    def test_placement(self):
        bldg = Building(num_floors=4)


class EquipmentTest(unittest.TestCase):
    def test_ctx(self):
        name = 'name'
        compat = 'compat'
        e = Equipment(name=name, compatibility=compat)
        self.assertEqual(e.full_name, name)
        self.assertEqual(e.compatible_type, compat)
        self.assertEqual(e.short_name, (name[0].upper() + compat[0]).upper())
        e = Equipment()
        self.assertEqual(e.full_name, '')
        self.assertEqual(e.compatible_type, '')
        self.assertEqual(e.short_name, '')


class MicrochipTest(unittest.TestCase):
    def test_ctx(self):
        compat = 'compat'
        m = Microchip(compatibility=compat)
        self.assertEqual(m.full_name, Microchip.full_name)
        self.assertEqual(m.compatible_type, compat)
        self.assertEqual(m.short_name, 'MC')


class GeneratorTest(unittest.TestCase):
    def test_ctx(self):
        compat = 'compat'
        g = Generator(compatibility=compat)
        self.assertEqual(g.full_name, Generator.full_name)
        self.assertEqual(g.compatible_type, compat)
        self.assertEqual(g.short_name, 'GC')


class StorageClassTest(unittest.TestCase):
    def setUp(self):
        self.e1 = Equipment(name='name1', compatibility='compat1')
        self.e2 = Equipment(name='name2', compatibility='compat2')

    def test_ctx(self):
        sc = StorageClass()
        self.assertEqual(sc.equipment_list, [])
        sc = StorageClass(equipment=[self.e1])
        self.assertEqual(sc.equipment_list, [self.e1])
        with self.assertRaises(StorageError):
            StorageClass(equipment=[1, 2, 3])
        sc = StorageClass(equipment=[self.e1, self.e2])
        self.assertEqual(sc.equipment_list, [self.e1, self.e2])

    def test_accept(self):
        sc = StorageClass()
        sc.accept(self.e1)
        self.assertEqual(sc.equipment_list, [self.e1])
        sc.accept(self.e2)
        self.assertEqual(sc.equipment_list, [self.e1, self.e2])

    def test_drop(self):
        sc = StorageClass()
        sc.accept(self.e1)
        self.assertEqual(sc.equipment_list, [self.e1])
        sc.accept(self.e2)
        self.assertEqual(sc.equipment_list, [self.e1, self.e2])
        sc.drop(self.e1)
        self.assertEqual(sc.equipment_list, [self.e2])
        sc.drop(self.e2)
        self.assertEqual(sc.equipment_list, [])
        with self.assertRaises(StorageError):
            sc.drop(self.e1)
        with self.assertRaises(StorageError):
            sc.drop(Equipment())

    def test_item_count(self):
        sc = StorageClass()
        self.assertEqual(sc.item_count(), 0)
        sc.accept(self.e1)
        self.assertEqual(sc.item_count(), 1)
        sc.accept(self.e2)
        self.assertEqual(sc.item_count(), 2)
        sc.drop(self.e2)
        self.assertEqual(sc.item_count(), 1)
        sc.drop(self.e1)
        self.assertEqual(sc.item_count(), 0)


class FloorTest(unittest.TestCase):
    def setUp(self):
        compat = 'phosphorus'
        self.gp = Generator(compatibility=compat)
        self.mp = Microchip(compatibility=compat)
        compat = 'sulfur'
        self.gs = Generator(compatibility=compat)
        self.ms = Microchip(compatibility=compat)

    def test_ctx(self):
        f = Floor(level=1)
        self.assertEqual(f.level, 1)

    def test_drop(self):
        f = Floor(level=1)
        f.accept(self.gp)
        self.assertIn(self.gp, f.equipment_list)
        self.assertEqual(f.item_count(), 1)
        f.drop(self.gp)
        self.assertNotIn(self.gp, f.equipment_list)
        self.assertEqual(f.item_count(), 0)


class ElevatorTest(unittest.TestCase):
    def setUp(self):
        compat = 'phosphorus'
        self.gp = Generator(compatibility=compat)
        self.mp = Microchip(compatibility=compat)
        compat = 'sulfur'
        self.gs = Generator(compatibility=compat)
        self.ms = Microchip(compatibility=compat)
        self.f1 = Floor(level=1)

    def test_ctx(self):
        e = Elevator(starting_floor=self.f1)
        self.assertEqual(e.cur_floor, self.f1)
        test_list = [self.gp, self.mp]
        e = Elevator(starting_floor=self.f1, equipment=test_list)
        self.assertEqual(e.equipment_list, test_list)

    def test_accept(self):
        self.f1.accept(self.gp)
        e = Elevator(starting_floor=self.f1)
        e.accept(self.gp)
        self.assertIn(self.gp, e.equipment_list)
        self.assertEqual(e.equipment_list, [self.gp])
        test_list = [self.mp, self.gs, self.ms]
        for equip in test_list:
            self.f1.accept(equip)
        e.accept(self.mp)
        self.assertEqual(e.equipment_list, [self.gp, self.mp])

    def test_drop(self):
        e = Elevator(starting_floor=self.f1, equipment=[self.gs, self.gp])
        e.drop(self.gs)
        self.assertEqual(e.equipment_list, [self.gp])
        self.assertEqual(self.f1.equipment_list, [self.gs])
