# Stdlib
from __future__ import print_function

import unittest
from collections import Counter

from adventofcode2016.utils.day4_helpers import *

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'

T = 'test'
N = 'name'
S = 'sector'
C = 'checksum'
R = 'real'


class RoomTest(unittest.TestCase):
    def setUp(self):
        self.tests = [{T: 'aaaaa-bbb-z-y-x-123[abxyz]', N: 'aaaaa-bbb-z-y-x', S: '123', C: 'abxyz', R: True},
                 {T: 'a-b-c-d-e-f-g-h-987[abcde]', N: 'a-b-c-d-e-f-g-h', S: '987', C: 'abcde', R: True},
                 {T: 'not-a-real-room-404[oarel]', N: 'not-a-real-room', S: '404', C: 'oarel', R: True},
                 {T: 'totally-real-room-200[decoy]', N: 'totally-real-room', S: '200', C: 'decoy', R: False},
                 ]

    def test_room_ctx(self):
        for test in self.tests:
            log.info('testing: [{}]'.format(test[T]))
            r = Room(test[T])
            self.assertEqual(r.encrypted_name, test[N])
            self.assertEqual(r.sector, test[S])
            self.assertEqual(r.checksum, test[C])
            self.assertEqual(r.is_real, test[R])
            self.assertTrue(count_checks(r.encrypted_name, r.char_count))

    def test_decrypt_name(self):
        test = 'qzmt-zixmtkozy-ivhz'
        sector = 343
        answer = 'very encrypted name'
        self.assertEqual(Room.decrypt_name(test, sector), answer)


def count_checks(s: str, d: dict):
    counter = Counter(s.replace('-', ''))
    for k, v in d.items():
        for c in v:
            count = k
            if counter[c] != count:
                return False
    return True
