"""
run_tests.py from adventofcode2016
Created: 12/1/2016
"""
# Stdlib
from __future__ import print_function
import os
import unittest

__author__ = 'jed.mitten'
__version__ = '0.0.1'


if __name__ == '__main__':
    tests_dir = os.path.join(os.path.split(__file__)[0], 'tests')
    suite = unittest.TestLoader().discover(start_dir=tests_dir)
    unittest.TextTestRunner().run(suite)
