"""
day4.py from adventofcode2016
Created: 12/7/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day4_helpers import Parser
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    inputs = get_input('day4part1.txt')
    rooms = Parser.parse(inputs)
    log.info('Read {} rooms from input'.format(len(rooms)))
    real_rooms = [r for r in rooms if r.is_real]
    sector_sum = 0
    for room in real_rooms:
        sector_sum += int(room.sector)
        if 'north' in room.name:
            print('Part 2: room {} has sector {}'.format(room.name, room.sector))
    print('Part 1: Sum of room sectors: {}'.format(sector_sum))

if __name__ == '__main__':
    main()
