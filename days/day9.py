"""
day9.py from adventofcode2016
Created: 12/9/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day9_helpers import Decompressor
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    challenge_input = get_input('day9.txt')
    decompressed = Decompressor.decompress(challenge_input)
    print('Part 1: Length of decompressed: {}'.format(len(decompressed)))
    decompressed_len = Decompressor.get_expanded_length(challenge_input)
    print('Part 2: Length of decompressed: {}'.format(decompressed_len))


if __name__ == '__main__':
    main()
