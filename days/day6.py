"""
day6.py from adventofcode2016
Created: 12/8/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day6_helpers import Parser
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    m_str = get_input('day6.txt')
    art = Parser.parse(m_str)
    print('Part 1: Message = |{}|'.format(art.message))
    art = Parser.parse(m_str, use_least_common=True)
    print('Part 2: Message = |{}|'.format(art.message))

if __name__ == '__main__':
    main()
