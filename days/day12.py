"""
day9.py from adventofcode2016
Created: 12/9/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day12_helpers import AssemblyProcessor
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    ap = AssemblyProcessor()
    input_str = get_input('day12.txt')
    instructions = [i for i in input_str.split('\n') if i]
    ap.instructions = instructions
    ap.process()
    print('Part 1: Register a contains: {}'.format(ap.a))
    ap = AssemblyProcessor()
    ap.c = 1
    ap.instructions = instructions
    ap.process()
    print('Part 2: Register a contains: {}'.format(ap.a))



if __name__ == '__main__':
    main()
