"""
day9.py from adventofcode2016
Created: 12/9/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day10_helpers import Parser
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    instructions = get_input('day10.txt')
    f = Parser.parse(instructions)
    check_high = 61
    check_low = 17
    for bot_name in f.bots:
        if f.bots[bot_name].prev_low == check_low and f.bots[bot_name].prev_high == check_high:
            print('Part 1: Bot {} is responsible for comparing {} and {}'.format(
                f.bots[bot_name].name, check_high, check_low))
    output0 = f.get_output(0)
    output1 = f.get_output(1)
    output2 = f.get_output(2)
    idx = 0
    v = output0.chips[idx] * output1.chips[idx] * output2.chips[idx]
    print('Part 2: Output{out0}.chips[{idx}] * Output{out1}.chips[{idx}] * Output{out2}.chips[{idx}] = {v}'
          .format(out0=output0.name, out1=output1.name, out2=output2.name, idx=idx, v=v))


if __name__ == '__main__':
    main()
