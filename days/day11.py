"""
day9.py from adventofcode2016
Created: 12/9/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day11_helpers import Building
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    bldg = 'x'


if __name__ == '__main__':
    main()
