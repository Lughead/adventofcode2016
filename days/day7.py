"""
day7.py from adventofcode2016
Created: 12/8/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day7_helpers import Parser
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    m_str = get_input('day7.txt')
    addrs = Parser.parse(m_str)
    log.info('Parsed {} addrs'.format(len(addrs)))
    support_tls_addrs = [a for a in addrs if a.supports_tls]
    print('Part 1: There are {} addrs that support TLS'.format(len(support_tls_addrs)))
    support_ssl_addrs = [a for a in addrs if a.supports_ssl]
    print('Part 2: There are {} addrs that support SSL'.format(len(support_ssl_addrs)))


if __name__ == '__main__':
    main()
