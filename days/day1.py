"""
day1.py from adventofcode2016
Created: 12/1/2016
"""
# Stdlib
from __future__ import print_function
import logging
import argparse

# Third Party code
# Custom Code
from adventofcode2016.utils.day1_helpers import Day1Parser

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'

# from personal advent of code input
INPUT = 'R4, R5, L5, L5, L3, R2, R1, R1, L5, R5, R2, L1, L3, L4, R3, L1, L1, R2, R3, R3, R1, L3, L5, R3, R1, L1, R1,' +\
        'R2, L1, L4, L5, R4, R2, L192, R5, L2, R53, R1, L5, R73, R5, L5, R186, L3, L2, R1, R3, L3, L3, R1, L4, L2,' +\
        'R3, L5, R4, R3, R1, L1, R5, R2, R1, R1, R1, R3, R2, L1, R5, R1, L5, R2, L2, L4, R3, L1, R4, L5, R4, R3, L5,' +\
        'L3, R4, R2, L5, L5, R2, R3, R5, R4, R2, R1, L1, L5, L2, L3, L4, L5, L4, L5, L1, R3, R4, R5, R3, L5, L4, L3,' +\
        'L1, L4, R2, R5, R5, R4, L2, L4, R3, R1, L2, R5, L5, R1, R1, L1, L5, L5, L2, L1, R5, R2, L4, L1, R4, R3, L3,' +\
        'R1, R5, L1, L4, R2, L3, R5, R3, R1, L3'


def get_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Show verbose output')
    return parser.parse_args()


def main(opts: argparse.Namespace):
    if not opts.verbose:
        logging.disable(logging.DEBUG)
    santa = Day1Parser.parse(INPUT)
    print('Part 1: Santa traveled {} blocks'.format(santa.distance))
    print('Part 2: Santa repeated a corner after traveling {} blocks'.format(santa.first_repeat_distance))


if __name__ == '__main__':
    opts = get_opts()
    main(opts)