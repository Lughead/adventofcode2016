"""
day7.py from adventofcode2016
Created: 12/8/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day8_helpers import Parser
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    instructions = get_input('day8.txt')
    screen = Parser.parse(instructions)
    print('Part 1: There are {} pixels lit'.format(len(screen.lit_pixels)))
    print('Part 2: Screen is displaying\n{}'.format(str(screen)))

if __name__ == '__main__':
    main()
