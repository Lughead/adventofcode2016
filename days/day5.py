"""
day4.py from adventofcode2016
Created: 12/7/2016
"""
# Stdlib
from __future__ import print_function
import logging

# Third Party code
# Custom Code
from adventofcode2016.utils.day5_helpers import DoorCode, ComplexDoorCode
from adventofcode2016.utils import get_input

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
__author__ = 'jed.mitten'
__version__ = '0.0.1'


def main():
    salt = get_input('day5.txt')
    log.info('Read salt {} from input'.format(salt))
    # code = DoorCode(salt=salt)
    # print('Part 1: The passcode is: {}'.format(code.password))
    code = ComplexDoorCode(salt=salt)
    print('Part 2: The passcode is: {}'.format(code.password))

if __name__ == '__main__':
    main()
